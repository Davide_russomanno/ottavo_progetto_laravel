<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\PortraitsController;





Route::get('/', [PublicController::class, 'home'])->name('home');

Route::get('/portrait/create', [PortraitsController::class, 'create'])->name('portrait.create');

Route::post('/portrait/store', [PortraitsController::class, 'store'])->name('portrait.store');

Route::get('/portrait/tecnic/{portrait}', [PortraitsController::class, 'getPortraitByTecnic'])->name('portrait.tecnic');

Route::get('/portrait/show/{portrait}', [PortraitsController::class, 'show'])->name('portrait.show');

Route::get('/user/profile', [PublicController::class, 'profile'])->name('user.profile');





