<?php

namespace App\Http\Controllers;

use App\Models\Portrait;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home() {
        $portraits = Portrait::orderBy('created_at', 'desc')->get();
        return view('welcome', ['portraits' => $portraits]);
    }

    public function profile(){
        return view('profile');
    }
}
