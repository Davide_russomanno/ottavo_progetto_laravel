<?php

namespace App\Http\Controllers;

use App\Models\Tecnic;
use Illuminate\Http\Request;

class TecnicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tecnic  $tecnic
     * @return \Illuminate\Http\Response
     */
    public function show(Tecnic $tecnic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tecnic  $tecnic
     * @return \Illuminate\Http\Response
     */
    public function edit(Tecnic $tecnic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tecnic  $tecnic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tecnic $tecnic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tecnic  $tecnic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tecnic $tecnic)
    {
        //
    }
}
