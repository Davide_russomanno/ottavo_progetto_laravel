<?php

namespace App\Http\Controllers;

use App\Models\Tecnic;
use App\Models\Portrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PortraitRequest;

class PortraitsController extends Controller
{
    public function create(){
        $tecnics = Tecnic::all();
        return view('portraits.create', compact('tecnics'));
    }

    public function store(PortraitRequest $request){
        Portrait::create(
            [
                'name' => $request->input('name'),
                'price' => $request->input('price'),
                'img' => $request->file('img')->store('public/portraits'),
                'description' => $request->input('description'),
                'user_id' => Auth::user()->id,
                'tecnic_id' => $request->input('tecnic_id'),
            ]
        );

        return redirect()->route('home')->with('message', 'Grazie per aver caricato un nuovo dipinto');
    }

    public function getPortraitByTecnic(Portrait $portrait){
        $portraits = Portrait::all();
        return view('portraits.detail', compact('portrait', 'portraits'));
    }

    public function show(Portrait $portrait){
        return view('portraits.show', compact('portrait'));
    }
}
