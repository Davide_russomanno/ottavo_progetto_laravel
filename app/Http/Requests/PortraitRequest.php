<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PortraitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:50', 
            'tecnic_id' => 'required',
            'price' => 'required|numeric|min:1|max:999999',
            'img' => 'required',
            'description' => 'required|min:3|max:50000',
        ];
    }

    public function messages(){
        return[
            'name.required' => 'Il nome è obbligatorio',
            'name.min' => 'Il nome deve avere almeno 3 caratteri',
            'name.max' => 'Il nome deve avere al massimo 50 caratteri',

            'tecnic_id.required' => 'La tecnica è obbligatoria',

            'price.required' => 'Il prezzo è obbligatorio',
            'price.number' => 'Il prezzo deve essere di tipo numerico',
            'price.min' => 'Il prezzo deve avere almeno 1 carattere',
            'price.max' => 'Il prezzo deve avere al massimo 999999 caratteri',

            'img.required' => 'L immagine è obbligatoria',

            'description.required' => 'La descrizione è obbligatoria',
            'description.min' => 'La descrizione deve avere almeno 3 caratteri',
            'description.max' => 'La descrizione deve avere al massimo 50000 caratteri',
        ];
    }
}
