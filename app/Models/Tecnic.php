<?php

namespace App\Models;

use App\Models\Portrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tecnic extends Model
{
    use HasFactory;
    protected $fillable = [ 'name', 'difficulty' ];

    public function portraits(){
        return $this->HasMany(Portrait::class);
    }
}
