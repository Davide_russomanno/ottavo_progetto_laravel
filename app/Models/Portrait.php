<?php

namespace App\Models;

use App\Models\User;
use App\Models\Tecnic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Portrait extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'price', 'img', 'description', 'user_id', 'tecnic_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tecnic(){
        return $this->belongsTo(Tecnic::class);
    }
}
