<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portraits', function (Blueprint $table) {
            $table->unsignedBigInteger('tecnic_id');
            $table->foreign('tecnic_id')->references('id')->on('tecnics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portraits', function (Blueprint $table) {
            $table->dropForeign(['tecnic_id']);
            $table->dropColumn('tecnic_id');
        });
    }
};
