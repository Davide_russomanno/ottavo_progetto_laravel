<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Tecnic;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $tecnics = [
            'tratteggio' => 'facile',
            'sfumato' => 'medio',
            'puntinato' => 'facile',
            'acquerello' => 'difficile'
         ];
 
         foreach($tecnics as $name => $difficulty){
             Tecnic::create(
                 [
                     'name' => $name,
                     'difficulty' => $difficulty
                 ]
             );
         }
    }
}
