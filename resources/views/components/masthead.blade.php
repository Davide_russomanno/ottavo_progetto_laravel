<header class="masthead mb-4">
    <div class="container h-100"> 
    <div class="row h-100 align-items-center">     
      <div class="col-12 text-center color-text">
        <h1>Metti in vendita i tuoi dipinti</h1>
        <p>Il prezzo lo decidi tu!</p>
      </div>
    </div>
  </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#9c9c9c" fill-opacity="1" d="M0,288L24,261.3C48,235,96,181,144,160C192,139,240,149,288,160C336,171,384,181,432,181.3C480,181,528,171,576,149.3C624,128,672,96,720,101.3C768,107,816,149,864,160C912,171,960,149,1008,154.7C1056,160,1104,192,1152,208C1200,224,1248,224,1296,197.3C1344,171,1392,117,1416,90.7L1440,64L1440,320L1416,320C1392,320,1344,320,1296,320C1248,320,1200,320,1152,320C1104,320,1056,320,1008,320C960,320,912,320,864,320C816,320,768,320,720,320C672,320,624,320,576,320C528,320,480,320,432,320C384,320,336,320,288,320C240,320,192,320,144,320C96,320,48,320,24,320L0,320Z"></path>
    </svg>
</header>