<a href="{{ route('portrait.show', $portrait) }}" class="text-decoration-none">
<ul class="cards">
  <li>
    <div class="card">
      <img src="{{ Storage::url($portrait->img) }}" class="card__image" alt="{{ $portrait->name }}" />
      <div class="card__overlay">
        <div class="card__header">
          <svg class="card__arc" xmlns="http://www.w3.org/2000/svg"><path /></svg>                     
          <div class="card__header-text">
            <h3 class="card__title">{{ $portrait->name }}</h3> 
            <a href="{{ route('portrait.tecnic', $portrait) }}" class="text-decoration-none">
                <p class="card__status text-primary">Tecnica utilizzata: {{ $portrait->tecnic->name }}</p>
            </a>  
            <p class="card__status">Dipinto creato da: {{ $portrait->user->name }}</p>         
          </div>
        </div>
        <p class="card__description">{{ substr($portrait->description, 0, 20) }}</p>
      </div>
    </div>      
  </li>
</ul>
</a>