<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ route('home') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('portrait.create') }}">Crea ritratto</a>
        </li>
      </ul>
      <ul class="navbar-nav mx-auto">
        @if(Auth::user() == null)
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('register') }}">Registrati</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('login') }}">Accedi</a>
        </li>
        @else
        <a href="{{ route('user.profile') }}" class="text-decoration-none">
          <li class="nav-item fs-4">{{ Auth::user()->name }}</li>
        </a>
        <li class="nav-item fs-4 mx-5">
          <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button class="btn btn-primary">Logout</button>
          </form>
        </li>
        @endif
      </ul>
    </div>
  </div>
</nav>