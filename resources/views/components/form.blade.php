<form method="POST" action="{{ route('portrait.store') }}" enctype="multipart/form-data">
  @csrf
  <div class="mb-3">
    <label class="form-label">Nome ritratto</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
    @error('name')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Tecnica ritratto</label>
    <select name="tecnic_id" id="" class="form-control">
      @foreach($tecnics as $tecnic)
        <option value="{{ $tecnic->id }}">{{ $tecnic->name }}</option>
      @endforeach
    </select>
    @error('tecnic_id')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Prezzo</label>
    <input type="number" step="1" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}">
    @error('price')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Immagine</label>
    <input type="file" class="form-control @error('img') is-invalid @enderror" name="img" value="{{ old('img') }}">
    @error('img')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <div class="mb-3">
    <label class="form-label">Descrizione del ritratto</label>
    <textarea name="description" id="" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
    @error('description')
      <div class="text-danger mt-2">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Crea</button>
</form>