<x-layout>

<div class="container-fluid sfondo-user">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12 my-3">
                <h1>Bentornato {{ Auth::user()->name }}</h1>
            </div> 
        </div> 
        <div class="row my-2">
            @foreach(Auth::user()->portraits as $portrait)
            <div class="col-12 col-md-3">
                <x-card 
                :portrait="$portrait"
                />
            </div>
            @endforeach
        </div>
    </div>
</div>

</x-layout>

