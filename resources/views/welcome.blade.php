<x-layout>

    
    <x-masthead />
    
    @if (session('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('message') }}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

<div class="container-fluid sfondo-card">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1 class="d-flex justify-content-center">I migliori ritratti inseriti dai nostri utenti</h1>
            </div>
        </div>
        <div class="row my-5">
            @if($portraits->isNotEmpty())
                @foreach($portraits as $portrait)
                <div class="col-12 col-md-3">
                    <x-card 
                    :portrait="$portrait"
                    />
                </div>
                @endforeach
            @else
                <div class="col-12 col-md-3">
                    <h3>Non ci sono ritratti</h3>
                </div>
            @endif
        </div>
    </div>
</div>

</x-layout>