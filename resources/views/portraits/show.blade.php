<x-layout>
<div class="container-fluid sfondo-card">
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <h1>{{ $portrait->name }}</h1>
            </div>
        </div>
	  <div class="row my-3">
            <div class="col-12 col-md-6">
                <img src="{{ Storage::url( $portrait->img ) }}" alt="{{ $portrait->name }}">
            </div>
		 <div class="col-12 col-md-6">
                <h2>Tecnica utilizzata: <span class="fs-3">{{ $portrait->tecnic->name }}</span></h2>
                <h2 class="py-3">Prezzo: <span class="fs-3">€ {{ $portrait->price }}</span></h2>
                <h2>Descrizione: <span class="fs-3">{{ $portrait->description }}</span></h2>
            </div>
        </div>
    </div>
</div>

</x-layout>