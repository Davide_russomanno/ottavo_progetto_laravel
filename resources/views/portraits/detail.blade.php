<x-layout>

    <x-masthead />

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <h2>Ritratti creati con tecnica: {{ $portrait->tecnic->name }}</h2>
            </div>
        </div>
        <div class="row my-4">
                @foreach($portraits as $portrait)
                        <div class="col-12 col-md-3 my-2">
                            <x-card
                            
                                :portrait="$portrait"

                            />
                        </div>
                @endforeach
        </div>
    </div>

</x-layout>