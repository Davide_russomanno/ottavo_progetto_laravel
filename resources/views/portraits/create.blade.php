<x-layout>


    <x-masthead />
<div class="container-fluid sfondo-form">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="d-flex justify-content-center my-3">Riempi i campi per inserire il tuo ritratto</h1>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 col-md-6">
                <x-form :tecnics="$tecnics"/>
            </div>
            <div class="col-12 col-md-6">
                <div class="containers">
                    <div class="circle"></div>
                    <div class="circle"></div>
                    <div class="circle"></div>
                    <div class="shadow"></div>
                    <div class="shadow"></div>
                    <div class="shadow"></div>
                </div>
            </div>
        </div>
    </div>
</div>


</x-layout>